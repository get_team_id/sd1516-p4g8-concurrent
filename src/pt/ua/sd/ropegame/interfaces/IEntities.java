package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Contestant;
import pt.ua.sd.ropegame.memregions.Bench;
import pt.ua.sd.ropegame.memregions.Playground;
import pt.ua.sd.ropegame.memregions.RefereeSite;


public interface IEntities {

    void setRefSite(RefereeSite r);

    void setBench(Bench b);

    void setPlayground(Playground p);

    void updateMatchWinner(int winner, int[] results);

    void updateGameWinner(int currentGame, int gameWinner, int ntrials, boolean knockout);

    void updateContestantPosition(Contestant c);

    void updateTrial(int trial);

    void updateRopePosition(int ropePos);

    void generateLogFile();

    void removeContestantFromPosition(Contestant c, int pos);
}

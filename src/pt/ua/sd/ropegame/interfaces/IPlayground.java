package pt.ua.sd.ropegame.interfaces;

public interface IPlayground {

    int getRopePos();
    int getCurrentTrial();
}

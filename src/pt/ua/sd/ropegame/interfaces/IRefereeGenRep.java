package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Referee;
import pt.ua.sd.ropegame.enums.RefereeState;

public interface IRefereeGenRep extends IEntityGenRep {

    void updateRefState(RefereeState state);
    void updateTrialNumber(int n);

    void setRef(Referee referee);
}

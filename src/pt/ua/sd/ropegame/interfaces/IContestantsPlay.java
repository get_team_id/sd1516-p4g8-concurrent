package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Contestant;


public interface IContestantsPlay extends IPlayground {

    void standInLine(Contestant c) throws InterruptedException;
    void getReady(Contestant c) throws InterruptedException;
    void pullTheRope(Contestant c) throws InterruptedException;
    boolean amDone(Contestant c) throws InterruptedException;
}

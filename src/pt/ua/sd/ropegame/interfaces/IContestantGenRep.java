package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Contestant;
import pt.ua.sd.ropegame.enums.ContestantState;

public interface IContestantGenRep extends IEntityGenRep{


    void updateContestantState(ContestantState state, Contestant c);
    void removeContestantFromPosition(Contestant c, int pos);

}

package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Coach;

public interface ICoachBench {

    void reviewNotes(Coach coach, boolean startOfGame, int currentGame);

    void callContestants(Coach strategy) throws InterruptedException;

    void waitForCoachCall(Coach coach) throws InterruptedException;

}

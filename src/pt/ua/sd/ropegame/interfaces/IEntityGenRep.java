package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.memregions.Bench;
import pt.ua.sd.ropegame.memregions.Playground;
import pt.ua.sd.ropegame.memregions.RefereeSite;


public interface IEntityGenRep {
    RefereeSite getRefSite();
    Bench getBench();
    Playground getPlayground();
}

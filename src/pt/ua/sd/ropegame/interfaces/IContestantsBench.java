package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Contestant;

/**
 * Created by rui on 3/17/16.
 */
public interface IContestantsBench {

    int waitForContestantCall(Contestant c) throws InterruptedException;

    void seatDown(Contestant contestant);

}

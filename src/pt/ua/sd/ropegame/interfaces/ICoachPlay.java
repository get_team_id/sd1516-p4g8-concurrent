package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Coach;


public interface ICoachPlay extends IPlayground {

    int getCurrentTrial();

    boolean reviewNotes(Coach coach) throws InterruptedException;

    void moveCoachToPlayground(Coach coach) throws InterruptedException;
}

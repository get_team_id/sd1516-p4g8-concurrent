package pt.ua.sd.ropegame.interfaces;

public interface IRefPlay extends IPlayground{

    int startTrial();
    boolean assertTrialDecision() throws InterruptedException;
    void announceNewGame();

}

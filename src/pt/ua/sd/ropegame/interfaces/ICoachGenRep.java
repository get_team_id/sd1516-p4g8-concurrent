package pt.ua.sd.ropegame.interfaces;

import pt.ua.sd.ropegame.entities.Coach;
import pt.ua.sd.ropegame.enums.CoachState;


public interface ICoachGenRep extends IEntityGenRep{

    void updateCoachState(CoachState state, Coach c);
}

package pt.ua.sd.ropegame.interfaces;

public interface IRefSite {

    void announceNewGame(int currentGame) throws InterruptedException;

    void declareGameWinner(int currentGame, int gameWinner, int ntrials, boolean knockout);
    void declareMatchWinner(int winner, int[] results);

    void startTrial() throws InterruptedException;

}

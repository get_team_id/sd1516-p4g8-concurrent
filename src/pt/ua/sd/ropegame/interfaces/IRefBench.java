package pt.ua.sd.ropegame.interfaces;


public interface IRefBench {

    void callTrial() throws InterruptedException;

    void notifySeatedContestants();
}

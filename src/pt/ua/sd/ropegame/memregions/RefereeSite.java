package pt.ua.sd.ropegame.memregions;

import pt.ua.sd.ropegame.entities.Coach;
import pt.ua.sd.ropegame.entities.Referee;
import pt.ua.sd.ropegame.interfaces.*;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Monitor that allows for communication between the referee and the coach.
 */
public class RefereeSite implements IRefSite, ICoachRefSite {

    private Lock mutex;

    private Condition waitingForCoachAndPlayers;
    private boolean coachAndContestantsReady;

    private IEntities repository;

    /**
     * Constructor
     * @param repo General Repository.
     */
    public RefereeSite(IEntities repo) {

        repository = repo;
        mutex = new ReentrantLock();

        waitingForCoachAndPlayers = mutex.newCondition();
        repo.setRefSite(this);
        coachAndContestantsReady = false;

    }


    /**
     * Transition
     * @param currentGame current game
     */
    @Override
    public void announceNewGame(int currentGame) {

    }

    /**
     * Block the referee until the last coach informs her all contestants are ready.
     * @throws InterruptedException Thread was interrupted.
     */
    @Override
    public void startTrial() throws InterruptedException {

        mutex.lock();

        while(!coachAndContestantsReady)
            waitingForCoachAndPlayers.await();

        coachAndContestantsReady = false;

        mutex.unlock();
    }

    /**
     * Called by the coach of the last team to arrive at the playground to inform the referee the trial can start.
     */
    @Override
    public void informReferee() {

        mutex.lock();
        coachAndContestantsReady = true;
        waitingForCoachAndPlayers.signal();

        mutex.unlock();
    }


    /**
     * Update general repository with last game stats.
     * @param currentGame number of the game.
     * @param gameWinner game winner.
     * @param ntrials number of trials to win.
     * @param knockout true if the victory was achieved by knockout.
     */
    @Override
    public void declareGameWinner(int currentGame, int gameWinner, int ntrials, boolean knockout) {

        mutex.lock();
        repository.updateGameWinner(currentGame, gameWinner, ntrials, knockout);
        mutex.unlock();
    }

    /**
     * Update repository with match stats.
     * @param winner the team who won the match.
     * @param results number of victories each team had.
     */
    @Override
    public void declareMatchWinner(int winner, int[] results) {

        mutex.lock();

        repository.updateMatchWinner(winner, results);
        repository.generateLogFile();

        mutex.unlock();

    }

}

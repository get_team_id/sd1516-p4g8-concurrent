package pt.ua.sd.ropegame.memregions;

import genclass.GenericIO;
import pt.ua.sd.ropegame.entities.Coach;
import pt.ua.sd.ropegame.entities.Contestant;
import pt.ua.sd.ropegame.entities.Referee;
import pt.ua.sd.ropegame.enums.CoachState;
import pt.ua.sd.ropegame.enums.ContestantState;
import pt.ua.sd.ropegame.enums.RefereeState;
import pt.ua.sd.ropegame.interfaces.ICoachGenRep;
import pt.ua.sd.ropegame.interfaces.IContestantGenRep;
import pt.ua.sd.ropegame.interfaces.IEntities;
import pt.ua.sd.ropegame.interfaces.IRefereeGenRep;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * General repository: generates the log file.
 */
public class GeneralRepository implements IRefereeGenRep, IContestantGenRep, IEntities,
        ICoachGenRep {

    // entities
    private Referee ref;

    // memory regions
    private Playground playground;
    private Bench bench;
    private RefereeSite refereeSite;


    private Lock mutex;
    private String[] currentStatus;

    // positions to write data to.

    private enum STATUSID {
        REFSTAT(0),
        COACH1STAT(1), CONT01STAT(2), CONT02STAT(4), CONT03STAT(6), CONT04STAT(8), CONT05STAT(10),
        CONT01SG(3), CONT02SG(5), CONT03SG(7), CONT04SG(9), CONT05SG(11),
        COACH2STAT(12), CONT11STAT(13), CONT12STAT(15), CONT13STAT(17), CONT14STAT(19), CONT15STAT(21),
        CONT11SG(14), CONT12SG(16), CONT13SG(18), CONT14SG(20), CONT15SG(22),
        TEAM03(23), TEAM02(24), TEAM01(25), ROPE(26), TEAM11(27), TEAM12(28), TEAM13(29), NB(30), PS(31);


        private int id;

        STATUSID(int numVal) {
            this.id = numVal;
        }

    }

    // variables needed to create and write to a text file
    private Path file;
    ArrayList<String> lines;

    /**
     * Constructor for the general repository.
     */
    public GeneralRepository() {
        mutex = new ReentrantLock();

        currentStatus = new String[32];

        for (int i = 0; i < currentStatus.length; i++) {
            currentStatus[i] = new String("-");
        }

        currentStatus[STATUSID.ROPE.id] = ".";

        // create log file
        file = Paths.get("log.txt");
        lines = new ArrayList<>();

        printFirstLines();
    }


    /**
     * Print table titles.
     */
    private void printFirstLines() {

        String s1 = "Ref Coa 1 Cont 1 Cont 2 Cont 3 Cont 4 Cont 5 Coa 2 Cont 1 Cont 2 Cont 3 Cont 4 Cont 5    Trial";
        String s2 = "Sta Stat  Sta SG Sta SG Sta SG Sta SG Sta SG Stat  Sta SG Sta SG Sta SG Sta SG Sta SG 3 2 1 . 1 2 3 NB PS";
        lines.add(s1);
        lines.add(s2);
        GenericIO.writelnString(s1);
        GenericIO.writelnString(s2);
    }

    /**
     * @return refereeSite.
     */
    public RefereeSite getRefSite() {

        return this.refereeSite;
    }

    /**
     * @return bench.
     */
    @Override
    public Bench getBench() {
        return this.bench;
    }

    /**
     * @return playground
     */
    @Override
    public Playground getPlayground() {
        return this.playground;
    }

    /**
     * Set repository's refereeSite.
     * @param r an instance of RefereeSite.
     */
    @Override
    public void setRefSite(RefereeSite r) {
        mutex.lock();
        this.refereeSite = r;
        mutex.unlock();
    }

    /**
     * Set repository's bench.
     * @param b an instance of Bench.
     */
    @Override
    public void setBench(Bench b) {
        mutex.lock();
        this.bench = b;
        mutex.unlock();
    }

    /**
     * Set repository's playground.
     * @param p an instance of Playground.
     */
    @Override
    public void setPlayground(Playground p) {
        mutex.lock();
        this.playground = p;
        mutex.unlock();
    }

    /**
     * Displays "Match was won by team # (#-#). / was a draw." message.
     * @param winner team which won the match.
     * @param results final results.
     */
    @Override
    public void updateMatchWinner(int winner, int[] results) {

        mutex.lock();
        String s1;

        if(results[0] == results[1])
            s1 = "Match was a draw.";

        else {
            s1 = "Match was won by team " + winner + " ";
            s1 += "(" + results[0] + " - " + results[1] + ").";
        }

        lines.add(s1);
        GenericIO.writelnString(s1);
        mutex.unlock();
    }

    /**
     * Displays "Game # was won by team # by knock out in # trials. / by points. / was a draw." message.
     * @param currentGame current game.
     * @param gameWinner game winner.
     * @param ntrials current trial when game ended.
     * @param knockout true if one of the teams won by knockout.
     */
    @Override
    public void updateGameWinner(int currentGame, int gameWinner, int ntrials, boolean knockout) {
        mutex.lock();

        String s1 = "Game " + currentGame + " ";
        if(gameWinner == 0)
            s1 += "was a draw.";
        else {
            s1 += "was won by team ";
            if(knockout)
                s1 += gameWinner + " by knockout in " + ntrials + " trials.";

            else
                s1 += gameWinner + " by points.";
        }

        lines.add(s1);
        GenericIO.writelnString(s1);
        mutex.unlock();
    }


    /**
     * @return referee.
     */
    public Referee getRef() {
        return this.ref;
    }

    /**
     * Update referee state.
     * @param state Referee's new State.
     */
    @Override
    public void updateRefState(RefereeState state) {
        mutex.lock();

        currentStatus[STATUSID.REFSTAT.id] = state.shortName();
        printStatus();

        mutex.unlock();

    }

    /**
     * Set referee.
     * @param referee an instance of Referee.
     */
    @Override
    public void setRef(Referee referee) {
        mutex.lock();
        try {
            this.ref = referee;
        } finally {
            mutex.unlock();
        }
    }

    /**
     * Print current status.
     */
    private  void printStatus() {
        StringBuilder sb = new StringBuilder();

        for(String s: currentStatus) {
            sb.append(s);
            sb.append(" ");
        }

        String toPrint = sb.toString().trim();
        lines.add(toPrint);
        GenericIO.writelnString(toPrint);
    }


    /**
     * Update contestant state.
     * @param state new contestant state.
     * @param c the contestant we're refering to.
     */
    @Override
    public void updateContestantState(ContestantState state, Contestant c) {
        mutex.lock();

        try {
            if(c.getTeam() == 0) {
             switch (c.getNumber()) {
                 case 0:
                     currentStatus[STATUSID.CONT01STAT.id] = state.shortName();
                     currentStatus[STATUSID.CONT01SG.id] = c.getStrength() + " ";
                     break;
                 case 1:
                     currentStatus[STATUSID.CONT02STAT.id] = state.shortName();
                     currentStatus[STATUSID.CONT02SG.id] = c.getStrength() + " ";
                     break;
                 case 2:
                     currentStatus[STATUSID.CONT03STAT.id] = state.shortName();
                     currentStatus[STATUSID.CONT03SG.id] = c.getStrength() + " ";
                     break;
                 case 3:
                     currentStatus[STATUSID.CONT04STAT.id] = state.shortName();
                     currentStatus[STATUSID.CONT04SG.id] = c.getStrength() + " ";
                     break;
                 case 4:
                     currentStatus[STATUSID.CONT05STAT.id] = state.shortName();
                     currentStatus[STATUSID.CONT05SG.id] = c.getStrength() + " ";
                     break;
             }


            }
            else {
                switch (c.getNumber()) {
                    case 0:
                        currentStatus[STATUSID.CONT11STAT.id] = state.shortName();
                        currentStatus[STATUSID.CONT11SG.id] = c.getStrength() + "";
                        break;
                    case 1:
                        currentStatus[STATUSID.CONT12STAT.id] = state.shortName();
                        currentStatus[STATUSID.CONT12SG.id] = c.getStrength() + "";
                        break;
                    case 2:
                        currentStatus[STATUSID.CONT13STAT.id] = state.shortName();
                        currentStatus[STATUSID.CONT13SG.id] = c.getStrength() + "";
                        break;
                    case 3:
                        currentStatus[STATUSID.CONT14STAT.id] = state.shortName();
                        currentStatus[STATUSID.CONT14SG.id] = c.getStrength() + "";
                        break;
                    case 4:
                        currentStatus[STATUSID.CONT15STAT.id] = state.shortName();
                        currentStatus[STATUSID.CONT15SG.id] = c.getStrength() + "";
                        break;
                }

            }

            printStatus();
        } finally {
            mutex.unlock();
        }
    }

    /**
     * Update trial number.
     * @param n new trial number.
     */
    @Override
    public void updateTrialNumber(int n) {
        mutex.lock();

        currentStatus[STATUSID.NB.id] = String.valueOf(n);

        mutex.unlock();
    }

    /**
     * Update Coach State
     * @param state new coach state.
     * @param c the coach with whom this state is related.
     */
    @Override
    public void updateCoachState(CoachState state, Coach c) {
        mutex.lock();


        if(c.getTeam() == 0)
            currentStatus[STATUSID.COACH1STAT.id] = state.shortName();
        else
            currentStatus[STATUSID.COACH2STAT.id] = state.shortName();

        printStatus();

        mutex.unlock();

   }

    /**
     * Remove a contestant from a playground position. (unused)
     * @param c contestant
     * @param pos playground position
     */
    @Override
    public void removeContestantFromPosition(Contestant c, int pos) {
        int team = c.getTeam();

        switch (pos) {
            case 1:
                if(team == 0) currentStatus[STATUSID.TEAM03.id] = "-";
                else currentStatus[STATUSID.TEAM11.id] = "-";
                break;

            case 2:
                if(team == 0) currentStatus[STATUSID.TEAM02.id] = "-";
                else currentStatus[STATUSID.TEAM12.id] = "-";
                break;

            case 3:
                if(team == 0) currentStatus[STATUSID.TEAM01.id] = "-";
                else currentStatus[STATUSID.TEAM13.id] = "-";
                break;

            default: break;
        }
    }

    /**
     * Move a contestant to playground.
     * @param c Contestant.
     */
    @Override
    public void updateContestantPosition(Contestant c) {
        mutex.lock();
        switch(c.getTeam()) {
            case 0:
                switch (c.getPlaygroundPos()) {
                        case 0: // no position
                            break;
                        case 1: currentStatus[STATUSID.TEAM03.id] = "   " + String.valueOf(c.getNumber()+1);
                            break;
                        case 2:currentStatus[STATUSID.TEAM02.id] = String.valueOf(c.getNumber()+1);
                            break;
                        case 3:currentStatus[STATUSID.TEAM01.id] = String.valueOf(c.getNumber()+1);
                            break;

                }
                break;
            case 1:
                 switch(c.getPlaygroundPos()) {
                    case 0:
                        break;
                    case 1:currentStatus[STATUSID.TEAM11.id] = String.valueOf(c.getNumber()+1);
                        break;
                    case 2:currentStatus[STATUSID.TEAM12.id] = String.valueOf(c.getNumber()+1);
                        break;
                    case 3:currentStatus[STATUSID.TEAM13.id] = String.valueOf(c.getNumber()+1);
                        break;
                }
               break;
        }

        printStatus();

        mutex.unlock();
    }

    /**
     * Update current trial number.
     * @param trial current trial number.
     */

    @Override
    public void updateTrial(int trial) {
        mutex.lock();

        currentStatus[STATUSID.NB.id] = String.valueOf(trial);
        printStatus();

        mutex.unlock();
    }

    /**
     * Update current rope position.
     * @param ropePos current rope position.
     */
    @Override
    public void updateRopePosition(int ropePos) {
        mutex.lock();

        currentStatus[STATUSID.PS.id] = " " + String.valueOf(ropePos);
        printStatus();

        mutex.unlock();
    }

    /**
     * Generate final log file.
     */
    @Override
    public void generateLogFile() {
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


package pt.ua.sd.ropegame.memregions;

import pt.ua.sd.ropegame.entities.Coach;
import pt.ua.sd.ropegame.entities.CoachStrategies;
import pt.ua.sd.ropegame.entities.Contestant;
import pt.ua.sd.ropegame.interfaces.ICoachBench;
import pt.ua.sd.ropegame.interfaces.IContestantsBench;
import pt.ua.sd.ropegame.interfaces.IEntities;
import pt.ua.sd.ropegame.interfaces.IRefBench;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Memory region which stores information on both teams.
 */
public class Bench implements ICoachBench, IContestantsBench, IRefBench {

    private static final Random RANDOMGEN = new Random();

    // number of coaches
    private int nCoaches;

    // contestants and coaches
    Contestant[][] contestants;
    Coach[] coaches;

    private GeneralRepository gr;

    private boolean[] coachPickedContestants;

    // number of contestants in bench
    private int numTeamContestants[];

    // variable to assure that coaches go from WFRC to ASTE
    private int nCoachesBeingCalled;

    private int currentTrial;

    private Condition[] waitingForTeamContestants;

    private Lock mutex;                     // assures mutual exclusion
    private Condition[][] waitingForPick;
    private Condition waitingForTrialToStart;
    private boolean gameIsOver;

    private final int MAX_TRIALS = 6;
    private final int MAX_GAMES = 3;


    /**
    *  Constructor for the bench.
     */
    public Bench(IEntities rep) {

        mutex = new ReentrantLock();

        gameIsOver = false;

        waitingForTeamContestants = new Condition[2];
        waitingForTeamContestants[0] = mutex.newCondition();
        waitingForTeamContestants[1] = mutex.newCondition();


        waitingForPick = new Condition[2][5];
        for(int i = 0; i < waitingForPick.length; i++)
            for(int j = 0; j < waitingForPick[i].length; j++)
                waitingForPick[i][j] = mutex.newCondition();

        numTeamContestants = new int[2];
        numTeamContestants[0] = numTeamContestants[1] = 0;    // all contestants in bench at the start of the game

        rep.setBench(this);

        nCoachesBeingCalled = 0;
        currentTrial = 0;

        nCoaches = 0;

        waitingForTrialToStart = mutex.newCondition();

        coachPickedContestants = new boolean[2];
        coachPickedContestants[0] = false;
        coachPickedContestants[1] = false;

    }

    /**
     * Called by referee to signal a new trial call.
     */
    @Override
    public void callTrial() {
        mutex.lock();
        waitingForTrialToStart.signalAll();
        mutex.unlock();

    }

    /**
     * The coach is blocked in state WAITING_FOR_REFEREE_COMMAND.

     * @param coach The coach who's waiting for the call.
     * @throws InterruptedException The wait was interrupted.
     */
    @Override
    public void waitForCoachCall(Coach coach) throws InterruptedException {

        mutex.lock();

        nCoachesBeingCalled++;
        while(nCoachesBeingCalled < 2)
            waitingForTrialToStart.await();
        waitingForTrialToStart.signalAll();

        mutex.unlock();

    }

    /**
     * Updates this coaches' team players strengths.
     * @param coach The coach whose team contestants we want to update strengths.
     * @param startOfGame Boolean that signals if we're in the start of a game.
     * @param currentGame Current game.
     */
    @Override
    public void reviewNotes(Coach coach, boolean startOfGame, int currentGame) {
        mutex.lock();

        int team = coach.getTeam();
        nCoaches++; // increment the number of coaches reviewing notes

        int prevTrial = currentTrial +1;

        // reset coaches waiting for coach call
        if(nCoaches == 1) nCoachesBeingCalled = 0;
        if(nCoaches == 2) {
            if(startOfGame) currentTrial = 0;   // resets current trial number

            currentTrial++;
            nCoaches = 0;   // resets number of coaches reviewing notes
        }

        for(Contestant c: contestants[team]) {
            if(prevTrial > 1 || currentGame > 1)    // check when to update strength
                c.updateStrength();

        }


        mutex.unlock();
    }

    /**
     * Picks team contestants based on coach's current strategy.
     * @param coach The coach whose contestants we're calling.
     * @throws InterruptedException The thread was interrupted.
     */
    @Override
    public void callContestants(Coach coach) throws InterruptedException {

        mutex.lock();

        int team = coach.getTeam();
        CoachStrategies.Strategy strategy = coach.getStrategy();

        switch (strategy) {
            case RANDOM:
                Set<Integer> generated = new HashSet<>();
                while (generated.size() < 3)
                {
                    Integer n = RANDOMGEN.nextInt(4);
                    generated.add(n);
                }

                for(int number: generated) {
                    for (Contestant c : contestants[team])
                        if (c.getNumber() == number) {
                            c.callContestant(true);
                            waitingForPick[team][number].signal();
                        }
                }

                break;

            case PICK_FIRST_THREE_CONTESTANTS:
                for(Contestant c: contestants[team]) {
                    if(c.getNumber() <= 2) {
                        c.callContestant(true);
                        waitingForPick[team][c.getNumber()].signal();
                    }
                }

                break;

            case PICK_LAST_THREE_CONTESTANTS:
                for(Contestant c: contestants[team]) {
                    if(c.getNumber() >= 2) {
                        c.callContestant(true);
                        waitingForPick[team][c.getNumber()].signal();
                    }
                }

                break;

        }


        mutex.unlock();

    }

    /**
     * Checks to see if a contestant was picked by his coach.
     * @param contestant The contestant who's waiting for coach call.
     * @return current trial number
     * @throws InterruptedException The thread was interrupted.
     */
    @Override
    public int waitForContestantCall(Contestant contestant) throws InterruptedException {

        mutex.lock();

        try {

            int team = contestant.getTeam();
            int num = contestant.getNumber();

            while(!(contestant.wasPicked()))
                waitingForPick[team][num].await();

            contestant.callContestant(false);

            if(gameIsOver) return MAX_TRIALS + 1;

            return currentTrial;

        } finally {
            mutex.unlock();
        }
    }

    /**
     * Called by the referee when a game is over to wake up contestants blocked at waitingForContestantCall().
     */
    @Override
    public void notifySeatedContestants()  {

        mutex.lock();

        gameIsOver = true;

        for(Contestant[] team: contestants)
            for(Contestant c: team) {
                c.callContestant(true);
                waitingForPick[c.getTeam()][c.getNumber()].signalAll();
        }

        mutex.unlock();
    }


    /**
     * Transition.
     * @param contestant
     */
    public void seatDown(Contestant contestant) {

    }


    /**
     * Assigns contestants to this bench.
     * @param contestants The two teams.
     */
    public void assignContestants(Contestant[][] contestants){
        mutex.lock();
        this.contestants = contestants;
        mutex.unlock();
    }

    /**
     * Assigns coaches to this bench.
     * @param coaches The coaches for both teams.
     */
    public void assignCoaches(Coach[] coaches){
        mutex.lock();
        this.coaches = coaches;
        mutex.unlock();
    }

}

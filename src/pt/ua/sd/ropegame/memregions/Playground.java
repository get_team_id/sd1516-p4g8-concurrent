package pt.ua.sd.ropegame.memregions;

import pt.ua.sd.ropegame.entities.Coach;
import pt.ua.sd.ropegame.entities.Contestant;
import pt.ua.sd.ropegame.interfaces.ICoachPlay;
import pt.ua.sd.ropegame.interfaces.IContestantsPlay;
import pt.ua.sd.ropegame.interfaces.IEntities;
import pt.ua.sd.ropegame.interfaces.IRefPlay;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Memory region where 3 players of each team will try to win the Game of the Rope.
 */
public class Playground implements ICoachPlay, IContestantsPlay, IRefPlay {

    private int currentTrial;

    private Condition waitingForTrialToStart;
    private Lock mutex;
    private Condition coachWaitingForContestants;

    private int nCoachesInPlayground;
    private int nContInPlayground;

    private boolean trialStarted;
    private boolean decided;

    private int playgroundPosTeam1, playgroundPosTeam2;
    private int ropePos;
    private boolean knockout;

    private int contestantsDone;
    private Condition issuingTrial;
    private Condition waitingForTrialDecision;

    private int nContestantsReady;

    private final int MAX_TRIALS = 6;
    private final int MAX_GAMES = 3;

    private int teamStrength[];

    private IEntities repository;

    private static final Random RANDOMGEN = new Random();

    /**
     * Constructor for Playground.
     * @param repo general repository.
     */
    public Playground(IEntities repo) {

        mutex = new ReentrantLock();
        coachWaitingForContestants = mutex.newCondition();
        waitingForTrialToStart = mutex.newCondition();
        issuingTrial = mutex.newCondition();
        waitingForTrialDecision = mutex.newCondition();

        nCoachesInPlayground = 0;
        nContInPlayground = 0;

        playgroundPosTeam1 = playgroundPosTeam2 = ropePos = 0;

        trialStarted = false;
        decided = false;
        contestantsDone = 0;
        currentTrial = 0;

        nContestantsReady = 0;

        repo.setPlayground(this);
        repository = repo;

        teamStrength = new int[2];

    }

    /**
     * Move a contestant to the playground.
     * @param c A contestant.
     * @throws InterruptedException
     */
     public void standInLine(Contestant c) throws InterruptedException {

         mutex.lock();

         nContInPlayground++;

         teamStrength[c.getTeam()] += c.getStrength();


         int pos = c.getTeam() == 0 ? ++playgroundPosTeam1 : ++playgroundPosTeam2;
         c.assignPosition(pos);
         repository.updateContestantPosition(c);

         // if all players arrived to playground, coaches may be awaken
         if(nContInPlayground == 6) {
             coachWaitingForContestants.signalAll();
             playgroundPosTeam1 = 0;
             playgroundPosTeam2 = 0;
             decided = false;
         }

         mutex.unlock();

    }

    /**
     * Move coach to the playground and wait for all the contestants to arrive.
     * @param coach A Coach.
     * @throws InterruptedException If Thread was interrupted.
     */
    @Override
    public void moveCoachToPlayground(Coach coach) throws InterruptedException {

        mutex.lock();

        while(nContInPlayground < 6)
            coachWaitingForContestants.await();

        nCoachesInPlayground++;

        // if all coaches and contestants are in the playground, we can inform referee.
        coach.goInformReferee(nCoachesInPlayground == 2);

        if(nCoachesInPlayground == 2) {
            nCoachesInPlayground = 0;
            nContInPlayground = 0;
        }
        mutex.unlock();

    }

    /**
     * Wait for referee to start trial.
     * @param c A contestant.
     * @throws InterruptedException If Thread was interrupted.
     */
    @Override
    public void getReady(Contestant c) throws InterruptedException {

        mutex.lock();


        while (!trialStarted)
            waitingForTrialToStart.await();

        nContestantsReady++;
        if(nContestantsReady == 6) {
            trialStarted = false;
            nContestantsReady = 0;
        }

        mutex.unlock();


    }

    /**
     * Called by referee. Wakes up all enteties waiting for the trial to start.
     * @return current trial.
     */
    @Override
    public int startTrial() {
        mutex.lock();

        try {

            waitingForTrialToStart.signalAll();

            trialStarted = true;

            // reset current trial
            if(currentTrial == 6 || knockout)
                this.currentTrial = 0;

            currentTrial++;

            repository.updateTrial(currentTrial);

            return currentTrial;

        } finally {

            mutex.unlock();
        }

    }


    /**
     * Simulate that a rope is being pulled. Wait for a random interval.
     * @param c The contestant who's pulling the rope.
     * @throws InterruptedException The thread was interrupted.
     */
    @Override
    public void pullTheRope(Contestant c) throws InterruptedException {
        mutex.lock();
        int r = RANDOMGEN.nextInt(100)+1;
        c.sleep(r);

        mutex.unlock();
    }

    /**
     * Called every time a contestant finishes pulling the rope.<p>
     * The last contestant to perform this operation wakes up the referee.
     * @param c The contestant that's done.
     * @return true if game ended due to knockout.
     * @throws InterruptedException
     */
    @Override
    public boolean amDone(Contestant c) throws InterruptedException {

        mutex.lock();

        try {
            contestantsDone++;
            c.hasPlayedInLastTrial();

            // if this is the last contestant
            if (contestantsDone == 6)
                issuingTrial.signal();

            // wait for trial decision
            while (!decided)
                waitingForTrialDecision.await();

            return knockout;
        } finally {

            mutex.unlock();
        }
    }

    /**
     * Referee waits for all contestants to finish pulling the rope and updates general repository with rope position.
     * @return true if game ended due to knockout.
     * @throws InterruptedException
     */
    @Override
    public boolean assertTrialDecision() throws InterruptedException {
        mutex.lock();

       try {
           while (contestantsDone < 6)
               issuingTrial.await();


           ropePos = ropePos + (teamStrength[1] - teamStrength[0]);
           repository.updateRopePosition(ropePos);

           knockout = (Math.abs(ropePos) >= 4);
           teamStrength[1] = teamStrength[0] = 0;

           decided = true;
           waitingForTrialDecision.signalAll();
           if(contestantsDone == 6) contestantsDone = 0;

           return knockout;
       } finally {

           mutex.unlock();
       }

    }

    /**
     * Called by referee to reset rope position.
     */
    @Override
    public void announceNewGame() {
        mutex.lock();

        this.ropePos = 0;

        mutex.unlock();
    }

    /**
     * @return current trial.
     */
    @Override
    public int getCurrentTrial() {
        mutex.lock();
        try {
            return currentTrial;
        } finally {
            mutex.unlock();
        }
    }

    /**
     *
     * @return rope position.
     */
    @Override
    public int getRopePos() {
        mutex.lock();
        try {
            return ropePos;
        } finally {
            mutex.unlock();
        }
    }

    /**
     * Called by coach in the end of a trial to change strategy based on what happened during the previous trial.
     * @param coach The coach.
     * @return true if game ended due to knockout.
     * @throws InterruptedException The Thread was interrupted.
     */
    @Override
    public boolean reviewNotes(Coach coach) throws InterruptedException {
        mutex.lock();

        try {
            while (!decided)
                waitingForTrialDecision.await();

            if(coach.getTeam() == 0) {
                if(ropePos < 0) coach.changeStrategy(1);
                else if(ropePos == 0) coach.changeStrategy(0);
                else if (ropePos > 0) coach.changeStrategy(-1);
            }

            else if(coach.getTeam() == 1) {
                if(ropePos < 0) coach.changeStrategy(-1);
                else if(ropePos == 0) coach.changeStrategy(0);
                else if (ropePos > 0) coach.changeStrategy(1);
            }

            return knockout;
        } finally {
            mutex.unlock();
        }


    }

}

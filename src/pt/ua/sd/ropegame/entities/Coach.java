package pt.ua.sd.ropegame.entities;

import pt.ua.sd.ropegame.enums.CoachState;
import pt.ua.sd.ropegame.interfaces.ICoachBench;
import pt.ua.sd.ropegame.interfaces.ICoachGenRep;
import pt.ua.sd.ropegame.interfaces.ICoachPlay;
import pt.ua.sd.ropegame.interfaces.ICoachRefSite;

/**
 *  This class is an implementation of a Coach thread.<p>
 *
 *  The coach starts by waiting for the referee command to start a game.<p>
 *  After receiving this command, the coach picks his team according to an assigned strategy.<p>
 *  He then moves to playground, informs the referee the team is ready and waits for the trial to end.<p>
 *  This process is repeated until MAX_GAMES is reached.
 */
public class Coach extends TeamMember {

    // memory regions
    private ICoachBench bench;
    private ICoachGenRep repository;
    private ICoachPlay playground;
    private ICoachRefSite refSite;

    // current state
    private CoachState currentState;

    // coach strategies to follow during the game
    private CoachStrategies strategies;

    // this variable is set to true when the second coach arrives at the playground after picking his team
    private boolean shouldInformRef;

    // game logic
    private int currentTrial;
    private int currentGame;
    private final int MAX_TRIALS = 6;
    private final int MAX_GAMES = 3;

    // this variable is set to true if a game ended due to a knockout
    private boolean knockout;


    /**
     * Constructor for Coach
     * @param repository The general repository
     * @param team The team this coach belongs to
     * @param strategies The pool of strategies this coach follows during game
     */
    public Coach(ICoachGenRep repository, int team, CoachStrategies strategies) {
        super(team);

        this.strategies = strategies;

        // set memory regions
        this.repository = repository;
        bench = repository.getBench();
        playground = repository.getPlayground();
        refSite = repository.getRefSite();
    }

    /**
     * Coach lifecycle
     */
    @Override
    public void run() {

        boolean running = true;
        currentGame = 1;
        currentTrial = 0;
        knockout = false;


        // the coach is waiting for referee command
        bench.reviewNotes(this, true, currentGame);
        currentState = CoachState.WAIT_FOR_REFEREE_COMMAND;

        CoachState nextState = currentState;

        while(running) {
            // update coach state
            repository.updateCoachState(currentState, this);

            switch (currentState) {

                case WAIT_FOR_REFEREE_COMMAND:
                    try {

                        // stop the coach's lifecycle if we've reached MAX_GAMES + 1
                        if (currentGame > MAX_GAMES) {
                            running = false;
                            break;
                        }

                        // wait for referee to call this coach
                        bench.waitForCoachCall(this);

                        // call contestants
                        bench.callContestants(this);

                        nextState = CoachState.ASSEMBLE_TEAM;

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    break;


                case ASSEMBLE_TEAM:
                    try {

                        // the coach is moved to the playground
                        playground.moveCoachToPlayground(this);

                        // the last of the coaches informs the referee
                        // this variable is always set in the operation moveCoachToPlayground
                        if(shouldInformRef)
                            refSite.informReferee();

                        nextState = CoachState.WATCH_TRIAL;

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    break;


                case WATCH_TRIAL:
                    try {

                        // wait until the trial has not finished
                        knockout = playground.reviewNotes(this);
                        currentTrial = playground.getCurrentTrial();

                        // conditions to start a new game
                        if(currentTrial == MAX_TRIALS || knockout) {
                            currentGame++;      // update currentGame
                            currentTrial = 0;   // reset currentTrial
                        }

                        // update this team's contestants' strength
                        bench.reviewNotes(this, currentTrial == 0, currentGame);

                        nextState = CoachState.WAIT_FOR_REFEREE_COMMAND;

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    break;
            }

            currentState = nextState;

        }
    }

    /**
     * Dinamically changes the coach strategy based on the last trial's result.
     * @param trialResult The last trial's result. 1 if this team won, -1 if this team lost, 0 if draw.
     */
    public void changeStrategy(int trialResult) {
        this.strategies.trialFinished(trialResult);
    }

    /**
     * @return The current strategy this coach is following
     */
    public CoachStrategies.Strategy getStrategy() {
        return strategies.getStrategy();
    }

    /**
     * This function is called in playground.moveCoachToPlayground.
     * @param b Sets if a coach should or should not inform the referee
     */
    public void goInformReferee(boolean b) {
        shouldInformRef = b;
    }

}

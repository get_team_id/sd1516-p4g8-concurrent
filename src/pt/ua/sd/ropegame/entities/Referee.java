package pt.ua.sd.ropegame.entities;

import pt.ua.sd.ropegame.enums.RefereeState;
import pt.ua.sd.ropegame.interfaces.IRefBench;
import pt.ua.sd.ropegame.interfaces.IRefPlay;
import pt.ua.sd.ropegame.interfaces.IRefSite;
import pt.ua.sd.ropegame.interfaces.IRefereeGenRep;


/**
 *  This class is an implementation of a Referee thread.<p>
 *
 *  The referee starts by announcing a new game and then a new trial.<p>
 *  When all contestants and coaches are ready, the referee starts a new trial.<p>
 *  The referee is responsible for announcing which team won each trial, game and match.<p>
 *  This process is repeated until MAX_GAMES is reached.
 */
public class Referee extends Thread {

    private RefereeState currentState;

    // memory regions
    private IRefBench bench;
    private IRefPlay playground;
    private IRefSite refereeSite;

    private IRefereeGenRep repository;

    private int currentGame;
    private int currentTrial;

    // game stats
    private int[] teamTrialScores;
    private int[] teamGameScores;

    // position of the rope and information on knockout
    private int ropePos;
    private boolean knockout;

    // game logic
    private final int MAX_TRIALS = 6;
    private final int MAX_GAMES = 3;

    /**
     * Constructor for a referee.
     * @param repository The general repository.
     */
    public Referee(IRefereeGenRep repository) {
        repository.setRef(this);

        // assign memory regions
        this.repository = repository;
        this.refereeSite = repository.getRefSite();
        this.playground = repository.getPlayground();
        this.bench = repository.getBench();

        // init variables
        currentTrial = 0;
        currentGame = 0;
        ropePos = 0;

        knockout = false;

        teamTrialScores = new int[2];
        teamGameScores = new int[2];
        teamGameScores[0] = teamGameScores[1] = 0;
    }


    /**
     * Referee's lifecycle.
     */
    @Override
    public void run() {

        this.currentState = RefereeState.START_OF_THE_MATCH;

        RefereeState nextState = this.currentState;
        currentGame = 1;
        boolean running = true;

        while(running) {
            repository.updateRefState(currentState);
            switch (currentState) {

                case START_OF_THE_MATCH:                    // transition

                    try {
                        // announce a new game
                        this.refereeSite.announceNewGame(currentGame);
                        nextState = RefereeState.START_OF_A_GAME;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;

                case START_OF_A_GAME:

                    try {
                        // call trial
                        bench.callTrial();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    nextState = RefereeState.TEAMS_READY;
                    break;

                case TEAMS_READY:
                    try {
                        // start trial when both teams are ready
                        refereeSite.startTrial();
                        currentTrial = playground.startTrial();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    nextState = RefereeState.WAIT_FOR_TRIAL_CONCLUSION;
                    break;

                case WAIT_FOR_TRIAL_CONCLUSION:
                    try {
                        // wait for trial conclusion and update game stats
                        knockout = this.playground.assertTrialDecision();
                        ropePos = this.playground.getRopePos();

                        if(ropePos > 0)
                            teamTrialScores[1]++;

                         else if(ropePos < 0)
                            teamTrialScores[0]++;

                        if(currentTrial == MAX_TRIALS || knockout) {
                            nextState = RefereeState.END_OF_A_GAME;

                        } else {
                            if(ropePos > 0) teamTrialScores[1]++;
                            else if(ropePos < 0) teamTrialScores[0]++;

                            bench.callTrial();
                            nextState = RefereeState.TEAMS_READY;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;

                case END_OF_A_GAME:

                    // declare game winner

                    int gameWinner = (teamTrialScores[0] != teamTrialScores[1]) ? ((teamTrialScores[0] > teamTrialScores[1]) ? 1 : 2) : 0;

                    teamTrialScores[0] = teamTrialScores[1] = 0;

                    switch (gameWinner){
                        case 0:
                            break;
                        case 1:
                            teamGameScores[0]++;
                            break;
                        case 2:
                            teamGameScores[1]++;
                            break;
                    }

                    refereeSite.declareGameWinner(currentGame, gameWinner, currentTrial, knockout);

                    // check if match has ended
                    if(currentGame == MAX_GAMES) {
                        // wake up contestants who were not picked
                        bench.notifySeatedContestants();
                        nextState = RefereeState.END_OF_THE_MATCH;
                    } else {

                        try {
                            playground.announceNewGame();
                            refereeSite.announceNewGame(++currentGame);
                            ropePos = 0;
                            nextState = RefereeState.START_OF_A_GAME;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case END_OF_THE_MATCH:
                    // declare match winner

                    int matchWinner = (teamGameScores[0] != teamGameScores[1]) ? ((teamGameScores[0] > teamGameScores[1]) ? 1 : 2) : 0;
                    refereeSite.declareMatchWinner(matchWinner, teamGameScores);
                    running = false;
                    break;

            }

            currentState = nextState;


        }

    }
}

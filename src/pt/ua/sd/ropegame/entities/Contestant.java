package pt.ua.sd.ropegame.entities;

import pt.ua.sd.ropegame.enums.ContestantState;
import pt.ua.sd.ropegame.interfaces.IContestantGenRep;
import pt.ua.sd.ropegame.interfaces.IContestantsBench;
import pt.ua.sd.ropegame.interfaces.IContestantsPlay;

import java.util.Random;

/**
 *  This class is an implementation of a Contestant thread.<p>
 *
 *  The contestant starts by waiting seated at the bench until he's picked by the coach.<p>
 *  The last contestant of a team to join the playground informs the coach that the full team is already on the playground and thus the trial can begin.<p>
 *  During the trial, every contestant pulls the rope and seats down afterward.<p>
 *  This process is repeated until MAX_GAMES is reached.
 */
public class Contestant extends TeamMember {

    // contestant information
    private int number;
    private int strength;
    private ContestantState currentState;
    private boolean picked;
    private int playgroundPos;
    private boolean playedInLastTrial;

    // memory regions
    private IContestantsBench bench;
    private IContestantsPlay playground;
    private IContestantGenRep repository;

    // game logic
    private int currentTrial;
    private boolean knockout;
    private int currentGame;

    private final int MAX_TRIALS = 6;
    private final int MAX_GAMES = 3;

    private final static Random RANDOMGEN = new Random();

    /**
     * Constructor for a contestant.
     * @param repository The general repository.
     * @param team The team this contestant belongs to.
     * @param number This contestant's number.
     */
    public Contestant(IContestantGenRep repository, int team, int number) {
        super(team);

        // generate a random strength value
        this.strength = RANDOMGEN.nextInt(5-1)+1;

        this.number = number;

        // assign memory regions
        this.repository = repository;
        bench = repository.getBench();
        playground = repository.getPlayground();

        playgroundPos = 0;
        playedInLastTrial = false;
        currentTrial = 0;
        picked = false;
        currentGame = 1;
    }



    @Override
    public void run() {

        this.currentState = ContestantState.SEAT_AT_THE_BENCH;
        boolean running = true;

        bench.seatDown(this);
        currentState = ContestantState.SEAT_AT_THE_BENCH;
        ContestantState nextState = currentState;

        while(running) {

            repository.updateContestantState(currentState, this);

            switch(currentState) {

                case SEAT_AT_THE_BENCH:

                    try {
                        // wait until the team's coach calls this contestant
                        currentTrial = bench.waitForContestantCall(this);

                        // conditions when the game should stop
                        if(currentTrial > MAX_TRIALS) {
                            running = false;
                            break;

                        }

                        // move to playground
                        playground.standInLine(this);
                        nextState = ContestantState.STAND_IN_POSITION;

                    } catch (InterruptedException e) {
                         e.printStackTrace();
                    }

                    break;

                case STAND_IN_POSITION:
                    try {
                        playground.getReady(this);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    nextState = ContestantState.DO_YOUR_BEST;
                    break;

                case DO_YOUR_BEST:
                    try {

                        playground.pullTheRope(this);
                        knockout = playground.amDone(this);

                        currentTrial = playground.getCurrentTrial();

                        if(currentTrial == MAX_TRIALS || knockout) {
                            currentGame++;
                            currentTrial = 0;
                        }

                        if(currentGame == MAX_GAMES+1) {
                            running = false;
                            break;
                        }

                        // seat down after the trial ended
                        bench.seatDown(this);
                        nextState = ContestantState.SEAT_AT_THE_BENCH;

                        //repository.removeContestantFromPosition(this, playgroundPos);
                        playgroundPos = 0;


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
            }

            currentState = nextState;

        }

    }

    /**
     * @return true if contestant played in last trial, false otherwise.
     */
    public boolean playedInLastTrial() {
        return playedInLastTrial;
    }


    /**
     * Sets the variable playedInLastTrial to true.
     */
    public void hasPlayedInLastTrial() {
        this.playedInLastTrial = true;
    }

    /**
     * @return This contestant's playground position.
     */
    public int getPlaygroundPos() {
        return this.playgroundPos;
    }

    /**
     * Call contestant.
     * @param pick true if the contestant was picked by the coach.
     */
    public void callContestant(boolean pick)  {

        picked = pick;
    }


    /**
     * @return true if the contestant was picked by the coach.
     */
    public boolean wasPicked() {
        return picked;
    }


    /**
     * @return This contestant's strength.
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Update contestant strength.
     */
    public void updateStrength() {
        if (playedInLastTrial) {
            if(this.strength > 0)
                this.strength -= 1;

            playedInLastTrial = false;

        }
        else
            this.strength += 1;
    }

    /**
     * @return This contestant's number.
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * Assign a playground position to this contestant.
     * @param playgroundPos Position this contestant occupies in the playground.
     */
    public void assignPosition(int playgroundPos) {
        this.playgroundPos = playgroundPos;
    }

}

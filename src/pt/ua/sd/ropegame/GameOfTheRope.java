package pt.ua.sd.ropegame;

import pt.ua.sd.ropegame.entities.Coach;
import pt.ua.sd.ropegame.entities.CoachStrategies;
import pt.ua.sd.ropegame.entities.Contestant;
import pt.ua.sd.ropegame.entities.Referee;
import pt.ua.sd.ropegame.memregions.Bench;
import pt.ua.sd.ropegame.memregions.GeneralRepository;
import pt.ua.sd.ropegame.memregions.Playground;
import pt.ua.sd.ropegame.memregions.RefereeSite;

/**
 *
 */
public class GameOfTheRope {

    public static void main(String... args) {

        int nTeams = 2;
        int nContestants = 5;

        // create general repository
        GeneralRepository repository = new GeneralRepository();

        // create memory regions
        Bench b = new Bench(repository);
        Playground playground = new Playground(repository);
        RefereeSite rs = new RefereeSite(repository);

        // create game entities
        Referee referee = new Referee(repository);

        Coach[] coaches = new Coach[nTeams];
        Contestant[][] teamContestants = new Contestant[nTeams][nContestants];

        for(int i = 0; i < nTeams; i++)
            coaches[i] =  new Coach(repository, i, new CoachStrategies());

        for(int i = 0; i < nTeams; i++) {

            for (int j = 0; j < nContestants; j++)
                teamContestants[i][j] = new Contestant(repository, i, j);

        }

        // assign coaches and contestants to bench
        b.assignCoaches(coaches);
        b.assignContestants(teamContestants);

        // start threads

        referee.start();

        for(int i = 0; i < nTeams; i++)
            coaches[i].start();
        for(int i = 0; i < nTeams; i++) {

            for (int j = 0; j < nContestants; j++)
                teamContestants[i][j].start();
        }

        try {
            referee.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < nTeams; i++)
            try {
                coaches[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        for(int i = 0; i < nTeams; i++) {
            for (int j = 0; j < nContestants; j++)
                try {
                    teamContestants[i][j].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
    }
}

# Game of the Rope #

This is a concurrent solution of the classic Game of the Problem, in which we have as entities the referee and the teams, which are composed by a coach and 5 contestants. They access three shared regions (Bench,Playground,Referee Site) to perform multiple operations, in order to conclude a sequence of three games, each of them consisting of 6 trials. 
All shared regions are connected to one shared region, the General Repository. 

This project is the first of three projects from the Distributed Systems course, from the 4th year of the Integrated Masters in Computers and Telematics Engineering (University of Aveiro).

### Main keywords ###

* Java
* Concurrency
* Explicit monitors

### Running ###

* Run GameOfTheRope.java

### The team ###

The entire solution was developped by Rui Espinha Ribeiro ([Espinha](https://bitbucket.org/Espinha)) and David Silva ([dmpasilva](https://bitbucket.org/dmpasilva)).